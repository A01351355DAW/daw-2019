<?php
  session_start();
  require_once("model.php");  

  $_POST["lugar"] = htmlspecialchars($_POST["lugar"]);
  $_POST["incidente_id"] = htmlspecialchars($_POST["incidente_id"]); 

  if(isset($_POST["lugar"])) {
      if (editar_incidente($_POST["incidente_id"], $_POST["lugar"])) {
          $_SESSION["mensaje"] = "Se editó el incidente";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al editar el incidente";
      }
  }

  header("location:index.php");
?>