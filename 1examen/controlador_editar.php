<?php 
  session_start();
  require_once("model.php");  

  $incidente_id = htmlspecialchars($_GET["incidente_id"]);

  $titulo = "Editar el incidente ".$incidente_id;
  include("_header.html");

  $lugar_id = recuperar_lugar($incidente_id);
  $editar = 1;
  include("_form_incidente.html"); 

  include("_footer.html"); 
?>