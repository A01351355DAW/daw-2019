<?php 
  //función para conectarnos a la BD
  function conectar_bd() {
      $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_1351355","1351355","dawbdorg_A01351355");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }

  //Consulta los incidentes de coronavirus
  //@param $lugar: El lugar de donde proviene el incidente
  //@param $tipo: El tipo de la infección del incidente
  function consultar_incidentes($lugar="", $tipo="") {
    $conexion_bd = conectar_bd();  
    
    $resultado =  "<table><thead><tr><th>Incidente</th><th>Lugar</th><th>Tipo</th><th>Fecha y hora</th></tr></thead>";
    
    $consulta = 'Select incidente_id, l.nombre as l_nombre, t.nombre as t_nombre, p.created_at as p_created_at 
    From tipo as t, posee as p, lugar as l, incidente as i 
    WHERE t.id = p.tipo_id 
    AND i.id = p.incidente_id 
    AND i.lugar_id = l.id
    ORDER BY p_created_at DESC';
    if ($lugar != "") {
        $consulta .= " AND lugar_id=".$lugar;
    }
    if ($tipo != "") {
        $consulta .= " AND tipo_id=".$tipo;
    }
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= '<td><a href="controlador_editar.php?incidente_id='.$row['incidente_id'].'">'.$row['incidente_id']."</a></td>"; //Se puede usar el índice de la consulta
        $resultado .= "<td>".$row['l_nombre']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['t_nombre']."</td>";
        $resultado .= "<td>".$row['p_created_at']."</td>";
        $resultado .= "</tr>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }

  //Crea un select con los datos de una consulta
  //@param $id: Campo en una tabla que conposee el id
  //@param $columna_descripcion: Columna de una tabla con una descripción
  //@param $tabla: La tabla a consultar en la bd
  function crear_select($id, $columna_descripcion, $tabla, $seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '<div class="input-field"><select name="'.$tabla.'"id="'.$tabla.'"><option value="" disabled selected>Selecciona una opción</option>';
            
    $consulta = "SELECT $id, $columna_descripcion FROM $tabla ORDER BY $tabla.`id` ASC";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<option value="'.$row["$id"].'" ';
        if($seleccion == $row["$id"]) {
            $resultado .= 'selected';
        }
        $resultado .= '>'.$row["$columna_descripcion"].'</option>';
    }
        
    desconectar_bd($conexion_bd);
    $resultado .=  '</select><label>'.$tabla.'...</label></div>';
    return $resultado;
  }

  //función para insertar un registro de incidente de coronavirus
  //@param lugar_id: id de la tabla lugar en la base de datos
 function insertar_incidente($lugar_id) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'INSERT INTO incidente (lugar_id) VALUES (?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i", $lugar_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }

    desconectar_bd($conexion_bd);
    return 1;
  }

  //Insterta el incidente en la tabla posee
  function insertar_completo($incidente_id,$tipo_id) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'INSERT INTO posee (incidente_id,tipo_id) VALUES (?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ii",$incidente_id,$tipo_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }

  //Funcion que devuelve id del incidente
  function consultar_idincidente() {
    $conexion_bd = conectar_bd();  
    
    $resultado = "";
    
    $consulta = 'Select id From incidente ORDER BY `incidente`.`id` ASC';
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado = $row['id'];
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
    return $resultado;
  }

  //Borrara un registro basandose en su id
  function borrar_registro_id($id){
    $conexion_bd = conectar_bd();

    $consulta = "DELETE FROM posee WHERE incidente_id = '".$id."'";

    $resultado = mysqli_query($conexion_bd,$consulta);

    $consulta = "DELETE FROM incidente WHERE id = '".$id."'";

    $resultado2 = mysqli_query($conexion_bd,$consulta);
    
    desconectar_bd($conexion_bd); 

    return ($resultado && $resultado2);
  }

  //función para editar un registro de incidente de coronavirus
  //@param incidente_id: id del incidente que se va a editar
  //@param lugar_id: id de la tabla lugar en la base de datos
  function editar_incidente($incidente_id, $lugar_id) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'UPDATE incidente SET lugar_id=(?) WHERE id=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ii", $lugar_id, $incidente_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }

  //Consultar el id del lugar a partir del id de un incidente
  //@param $incidente_id: El id del incidente
  function recuperar_lugar($incidente_id) {
    $conexion_bd = conectar_bd();  
      
    $consulta = "SELECT lugar_id FROM incidente WHERE id=$incidente_id";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["lugar_id"];
    }
        
    desconectar_bd($conexion_bd);
    return 0;
  }    
?>