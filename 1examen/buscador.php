<?php 
  session_start();
  require_once("model.php");  

  $titulo = "Buscador";
  include("_header.html");  

  include("_form.html"); 

  include("_btn_agregar.html");
  
  if (isset($_POST["lugar"])) {
      $lugar = htmlspecialchars($_POST["lugar"]);
  } else {
      $lugar = "";
  }

if (isset($_POST["tipo"])) {
      $tipo = htmlspecialchars($_POST["tipo"]);
  } else {
      $tipo = "";
  }
      echo '<div id="resultados_consulta">';
      echo consultar_incidentes($lugar,$tipo);
      echo '</div>';
 

  include("_footer.html"); 
?>