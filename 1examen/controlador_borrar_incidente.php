<?php
  session_start();
  require_once("model.php");  

  $_POST["incidente"] = htmlspecialchars($_POST["incidente"]);

  if(isset($_POST["incidente"])) {
      if (borrar_registro_id($_POST["incidente"])) {
          $_SESSION["mensaje"] = "Se borró el incidente";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al borrar el incidente";
      }
  }
  header("location:index.php");
?>