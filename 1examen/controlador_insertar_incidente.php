<?php 
	session_start();
  require_once("model.php");  

  $_POST["lugar"] = htmlspecialchars($_POST["lugar"]);
  $_POST["tipo"] = htmlspecialchars($_POST["tipo"]);

  if(isset($_POST["lugar"])) {
      insertar_incidente($_POST["lugar"]);
      if (insertar_incidente($_POST["lugar"])) {
          $_SESSION["mensaje"] = "Se registró el incidente";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al registrar el incidente";
      }
  }

  if(isset($_POST["tipo"])) {
    $id = consultar_idincidente();
    if (insertar_completo($id,$_POST["tipo"])) {
          $_SESSION["mensaje"] = "Se registró el incidente";
    } else {
          $_SESSION["warning"] = "Ocurrió un error al registrar el incidente";
    }
  }

  header("location:index.php");
?>