<?php
  session_start();
  require_once("../Models/model_UsuarioEditaRegistro.php");
  $folio = htmlspecialchars($_POST["folio"]);
  $nombre = htmlspecialchars($_POST["nombre"]);
  $apellidoP = htmlspecialchars($_POST["apellidoP"]);
  $apellidoM = htmlspecialchars($_POST["apellidoM"]);
  $sexo = htmlspecialchars($_POST["sexo"]);
  $edad = htmlspecialchars($_POST["edad"]);
  $telefono = htmlspecialchars($_POST["telefono"]);
  $email = htmlspecialchars($_POST["email"]);
  $handicap = htmlspecialchars($_POST["handicap"]);
  $indicacionEspecial = htmlspecialchars($_POST["indicacionEspecial"]);
  $metodopago = htmlspecialchars($_POST["metodoPago"]);
  $facturacion = htmlspecialchars($_POST["facturacion"]);
  $autorizarFotos= htmlspecialchars($_POST["autorizarFotos"]);
  $factRazonSocial= htmlspecialchars($_POST["factRazonSocial"]);
  $factRFC= htmlspecialchars($_POST["factRFC"]);
  $factDireccionFiscal= htmlspecialchars($_POST["factDireccionFiscal"]);
  $factIndicacionEsp= htmlspecialchars($_POST["factIndicacionEsp"]);
  $indicacionEspecialPago= htmlspecialchars($_POST["indicacionEspecialPago"]);
  $idParticipante= htmlspecialchars($_POST["idParticipante"]);

  echo editar_registroUsuario($folio,$nombre,$apellidoP,$apellidoM,$sexo,$edad,$telefono,$email,$handicap,$indicacionEspecial, $metodopago, $facturacion,$autorizarFotos,$factRazonSocial, $factRFC,$factDireccionFiscal,$factIndicacionEsp,$indicacionEspecialPago,$idParticipante);


?>