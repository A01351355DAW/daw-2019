<?php
    session_start();
    require_once("../Models/modelRegistroEquipo.php");
    $arrayJugadores = htmlspecialchars($_POST["arrayJugadores"]);
    $tamañoEquipo= htmlspecialchars($_POST["tamañoEquipo"]);
    $hoyoInicial = htmlspecialchars($_POST["hoyoInicial"]);
    $arrayNuevo = json_decode($arrayJugadores, true);
    echo registrarEquipo($arrayNuevo,$hoyoInicial,$tamañoEquipo);
?>