<?php
    session_start();
    require_once("../Models/modelEditarEquipo.php");
    $arrayAgregar = htmlspecialchars($_POST["arrayJugadoresAgregar"]);
    $arrayEliminar = htmlspecialchars($_POST["arrayJugadoresEliminar"]);
    $hoyoInicialEditar=htmlspecialchars($_POST["hoyoInicialEditar"]);
    $nuevoTamañoEquipo=htmlspecialchars($_POST["tamañoEquipo"]);
    $idEquipo=htmlspecialchars($_POST["idEquipoEditar"]);

    echo editarEquipo(json_decode($arrayAgregar, true),json_decode($arrayEliminar, true),$hoyoInicialEditar,$nuevoTamañoEquipo,$idEquipo);
    
?>