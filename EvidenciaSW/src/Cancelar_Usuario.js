
function cancelarRegistroUsuario() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    let circulo=document.getElementById("circuloCarga_CancelarUsuario");
    circulo.style.display="block";
    let forma=document.getElementById('formaCancelarUsuario');
    forma.style.display="none";
    $.post("../Controladores/controlador_cancelar_usuario.php", {
        
    }).done(function (data) {
        if(data==1){
            let forma=document.getElementById('formaCancelarUsuario');
            forma.style.display="none";
            let ajaxResponse=document.getElementById('mensajeEditarUsuario');
            let titulo=document.getElementById('confirmacionCancelarUsuario');
            let circulo=document.getElementById("circuloCarga_CancelarUsuario");
            circulo.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se canceló exitosamente el registro";
        }else{
            let ajaxResponse=document.getElementById('mensajeEditarUsuario');
            let titulo=document.getElementById('confirmacionCancelarUsuario');
            let forma=document.getElementById('formaCancelarUsuario');
            let circulo=document.getElementById("circuloCarga_CancelarUsuario");
            circulo.style.display="none";
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudo cancelar exitosamente el registro";
            
        }
    });
}




var habilitadaSeleccionarM=document.getElementById("botonCancelarUsuario");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = cancelarRegistroUsuario;
}