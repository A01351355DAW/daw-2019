function registrarPatrocinador(){
    if(!validarOrganizador()){
        return 0;
    }
    let circulo=document.getElementById("circuloCarga_RegPatro");
    let forma=document.getElementById('formaPatrocinador');
    let error=document.getElementById('encabezadoMensajePatrocinador');
    circulo.style.display="block";
    error.style.display="none";
    forma.style.display="none";
    $.post("../Controladores/controlador_registro_patrocinador.php", {
        nombre: $("#nombre_contacto").val(),
        nombreEmpresa: $("#empresa_nombre").val(),
        email: $("#empresa_correo").val(),
        descripcion: $("#descripcion_empresa").val(),
        telefono: $("#empresa_telefono").val(),
        Razonpatrocinio: $("#razon_patrocinio").val(),
    }).done(function (data) {
        if(data==1){
            let ajaxResponse=document.getElementById('mensajePatrocinador');
            let forma=document.getElementById('formaPatrocinador');
            let titulo=document.getElementById('tituloPatrocinador');
            let error=document.getElementById('encabezadoMensajePatrocinador');
            let circulo=document.getElementById("circuloCarga_RegPatro");
            circulo.style.display="none";
            error.style.display="none";
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se registró exitosamente los datos de patrocinio";
        }else{
            let ajaxResponse=document.getElementById('mensajePatrocinador');
            let titulo=document.getElementById('tituloPatrocinador');
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudo registrar los datos de patrocinio";
            let iconoRespuesta=document.getElementById('iconoRespuestaPatrocinador');
            iconoRespuesta.innerHTML="error";
            let error=document.getElementById('encabezadoMensajePatrocinador');
            error.style.display="none";
            let forma=document.getElementById('formaPatrocinador');
            forma.style.display="none";
            let circulo=document.getElementById("circuloCarga_RegPatro");
            circulo.style.display="none";
        }
       });
}

function validarOrganizador(){
    let telefono = $("#empresa_telefono").val();
    let nombreEmpresa = $("#empresa_nombre").val();
    let descripcion = $("#descripcion_empresa").val();
    let nombre= $("#nombre_contacto").val();
    let mail = $("#empresa_correo").val();
    let razonpatrocinio = $("#razon_patrocinio").val();
    if(nombreEmpresa.length < 2){
        let ajaxResponse=document.getElementById('encabezadoMensajePatrocinador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información válida en todos los campos</h5>";
        ajaxResponse.style.display="block";;
        return 0;
    }
    if(descripcion.length < 2){
        let ajaxResponse=document.getElementById('encabezadoMensajePatrocinador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información válida en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(nombre.length < 1){
        let ajaxResponse=document.getElementById('encabezadoMensajePatrocinador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información válida en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(mail.length < 5){
        let ajaxResponse=document.getElementById('encabezadoMensajePatrocinador');
        ajaxResponse.innerHTML="<h5>Por favor introduce información válida en todos los campos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    if(razonpatrocinio.length <2){
        let ajaxResponse=document.getElementById('encabezadoMensajePatrocinador');
        ajaxResponse.innerHTML='<h5>Por favor introduce información válida en todos los campos</h5>';
        ajaxResponse.style.display="block";
        return 0;
    }
    if(telefono.length < 10){
        let ajaxResponse=document.getElementById('encabezadoMensajePatrocinador');
        ajaxResponse.innerHTML="<h5>El teléfono  debe contener 10 dígitos</h5>";
        ajaxResponse.style.display="block";
        return 0;
    }
    return 1;
}

var habilitadaSeleccionarM=document.getElementById("boton_Registrar_Patro");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = registrarPatrocinador;
}