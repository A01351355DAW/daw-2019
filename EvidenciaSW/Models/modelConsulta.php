<?php
     
     function conectar_bd() {
      $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_torneo","torneo","dawbdorg_torneogolf");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }

  //Consulta los casos
  function consultar_casosEquipoTR($dato){
    $conexion_bd = conectar_bd();  
    
    $resultado = '<table class="centered"><thead><tr><th></th><th>Participante</th><th>Nombre Usuario</th><th>Nombre</th><th>Apellido Paterno</th><th>Apellido Materno</th><th>Handicap</th><th>Indicacion Especial</th></tr></thead>';
    
  
     $consulta = "SELECT P.idParticipante, P.nombreUsuario, P.nombre , P.apellidoPaterno, P.apellidoMaterno, P.handicap, PT.indicacionEspecial FROM participante_torneo as PT, participante as P, participante_equipo_torneo as PET where
     P.idParticipante=PT.idParticipante AND P.idParticipante NOT IN(select idParticipante from participante_equipo_torneo) AND (
    P.idParticipante LIKE '%".$dato."%' OR P.nombreUsuario LIKE '%".$dato."%' OR P.nombre LIKE '%".$dato."%' OR P.apellidoPaterno LIKE '%".$dato."%' OR P.apellidoMaterno LIKE '%".$dato."%' OR P.handicap LIKE '%".$dato."%' OR PT.indicacionEspecial LIKE '%".$dato."%')
     GROUP BY P.idParticipante";

    $resultados = $conexion_bd->query($consulta); 
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<tr id="participante'.$row['idParticipante'].'">';
        $resultado .= '<td> <a class="btn-floating btn-small waves-effect waves-light pink darken-1" id="boton'.$row['idParticipante'].'"><i onclick="añadirJugador('.$row['idParticipante'].'';
        $resultado .=',';
        $resultado.="'";
        $resultado.= $row['nombre'];
        $resultado.="'";
        $resultado.= ')"';
        $resultado .=' class="material-icons">add</i></a></td>';
        $resultado .= "<td>".$row['idParticipante']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombreUsuario']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombre']."</td>";
        $resultado .= "<td>".$row['apellidoPaterno']."</td>";
        $resultado .= "<td>".$row['apellidoMaterno']."</td>";
        $resultado .= "<td>".$row['handicap']."</td>";
        $resultado .= "<td>".$row['indicacionEspecial']."</td>";
        $resultado .= "</tr>";
    }
    mysqli_free_result($resultados); //Liberar la memoria
    desconectar_bd($conexion_bd);   
    $resultado .= "</tbody></table>";
    return $resultado;
  }
  
  function consultar_casosEquipoConsulta($dato){
    $conexion_bd = conectar_bd();  
    
    $resultado = '<table class="centered"><thead><tr><th>idEquipo</th><th>Participantes</th><th>Hoyo Inicial</th><th>Editar</th><th>Borrar</th></tr></thead>';
     $consulta = "Select PET.idEquipo, E.numParticipantes, E.hoyoInicial from participante_equipo_torneo as PET, equipo as E where
     PET.idEquipo=E.idEquipo AND(
      PET.idEquipo LIKE '%".$dato."%' OR E.numParticipantes LIKE '%".$dato."%' OR E.hoyoInicial LIKE '%".$dato."%') GROUP BY PET.idEquipo";

    $resultados = $conexion_bd->query($consulta); 
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
      $resultado .= "<tr>";
      $resultado .= "<td>".$row['idEquipo']."</td>"; //o el nombre de la columna
      $resultado .= "<td>".$row['numParticipantes']."</td>"; //o el nombre de la columna
      $resultado .= "<td>".$row['hoyoInicial']."</td>";
      $resultado .= '<td><a href="editarEquipo.php? equipo_id='.$row['idEquipo'].'" class="btn " id="botonesA"><i class="material-icons">create</i></a></td>'; //Se puede usar el índice de la consulta
      $resultado .= '<td><a href="cancelarRegistroUsuarioOrg.php" equipo_id='.$row['idEquipo'].'" class="btn " id="botones"><i class="large material-icons">clear</i></a></td>';
      $resultado .= "</tr>";
    }
    mysqli_free_result($resultados); //Liberar la memoria
    desconectar_bd($conexion_bd);   
    $resultado .= "</tbody></table>";
    return $resultado;
  }
  function consultar_casos() {
    $conexion_bd = conectar_bd();  
    
    $resultado =  "<table><thead><tr><th>Folio</th><th>Nombre Usuario</th><th>Nombre</th><th>Apellido Paterno</th><th>Apellido Materno</th><th>Handicap</th><th>Sexo</th><th>Pago</th><th>Estatus</th><th>Torneo</th><th>Editar</th><th>Cancelar</th></tr></thead>";
    
    $consulta = 'Select PT.idParticipante, folio, nombreUsuario, nombre , apellidoPaterno, apellidoMaterno, handicap, sexo, estatusPago, estatusInscripcion, idTorneo  from  participante as P, participante_torneo as PT WHERE PT.idParticipante=P.idParticipante ';
      
    $resultados = $conexion_bd->query($consulta); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= "<td>".$row['folio']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombreUsuario']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombre']."</td>";
        $resultado .= "<td>".$row['apellidoPaterno']."</td>";
        $resultado .= "<td>".$row['apellidoMaterno']."</td>";
        $resultado .= "<td>".$row['handicap']."</td>";
        $resultado .= "<td>".$row['sexo']."</td>";
        $resultado .= "<td>".$row['estatusPago']."</td>";
        $resultado .= "<td>".$row['estatusInscripcion']."</td>";
        $resultado .= "<td>".$row['idTorneo']."</td>";
        $resultado .= '<td><a href="editarRegistro.php?caso_id='.$row['folio'].'" class="btn " id="botones"><i class="material-icons">create</i></a></td>'; //Se puede usar el índice de la consulta
        $resultado .= '<td><a href="cancelarRegistroUsuarioOrg.php?caso_id='.$row['folio'].'&idP='.$row['idParticipante'].'" class="btn " id="botones"><i class="large material-icons">clear</i></a></td>';
        $resultado .= "</tr>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }


    function crear_select($id, $columna_descripcion, $tabla, $seleccion=0) {
        $conexion_bd = conectar_bd();  
          

        $resultado = '<select name="'.$tabla.'" id="'.$tabla.'"><option value="NA" disabled selected>Selecciona una opción</option>';       
        $consulta = "SELECT $id, $columna_descripcion FROM $tabla";
        if($tabla=="organizador"){
          $consulta.=" WHERE desactivado=0";
        }
        $resultados = $conexion_bd->query($consulta);
        while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
            $resultado .= '<option value="'.$row["$id"].'" ';
            $resultado .= '>'.$row["$columna_descripcion"].'</option>';
        }
            
        desconectar_bd($conexion_bd);
        $resultado .=  '</select>';
        return $resultado;
      }

  function crear_select_append($id, $columna_nombre, $columna_apellidoP, $columna_apellidoM, $tabla, $seleccion=0) {
        $conexion_bd = conectar_bd();  
          
        $resultado = '<select name="'.$tabla.'" id="selectN"><option value="NA" disabled selected>Selecciona una opción</option>';
                
        $consulta = "SELECT $id, $columna_nombre, $columna_apellidoP, $columna_apellidoM FROM $tabla";
        if($tabla=="organizador"){
          $consulta.=" WHERE desactivado=0";
        }
        $resultados = $conexion_bd->query($consulta);
        while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
            $resultado .= '<option value="'.$row["$id"].'" ';
            $resultado .= '>'.$row["$columna_nombre"]." ".$row["$columna_apellidoP"]." ".$row["$columna_apellidoM"].'</option>';
        }
            
        desconectar_bd($conexion_bd);
        $resultado .=  '</select>';
        return $resultado;
      }

  function consultar_patrocinadores($estatus,$orden){
     $conexion_bd = conectar_bd();  
    
    $resultado =  '<table class="highlight"><thead><tr><th>Nombre Contacto</th><th>Nombre Empresa</th><th>Teléfono</th><th>Correo</th><th>Descripción Empresa</th><th>Interés</th><th>Patrocinio Obtenido</th><th>Estado actual</th><th></th></tr></thead>';
    
    $consulta = 'Select nombreContacto, nombreEmpresa, telefono, correo, descripcionEmpresa,  descripcionInteres, estatusPatrocinio, patrocinioObtenido, fechaLarga FROM patrocinador';
    $consulta .= ' WHERE estatusPatrocinio="'.$estatus.'"';

    if ($orden == 1) {
        $consulta .= " ORDER BY fechaLarga DESC";
    }else{
        $consulta .= " ORDER BY fechaLarga ASC";
    }
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= "<td>".$row['nombreContacto']."</td>"; 
        $resultado .= "<td>".$row['nombreEmpresa']."</td>"; 
        $resultado .= "<td>".$row['telefono']."</td>";
        $resultado .= "<td>".$row['correo']."</td>";
        $resultado .= "<td>".$row['descripcionEmpresa']."</td>";
        $resultado .= "<td>".$row['descripcionInteres']."</td>";
        $resultado .= "<td>".$row['patrocinioObtenido']."</td>";
        $resultado .= "<td>".$row['estatusPatrocinio']."</td>";
        $resultado .= '<td> <a href="../Controladores/controlador_revisar_patrocinador.php?fecha='.$row['fechaLarga'].'&ptr='.$row['patrocinioObtenido'].'&empresa='.$row['nombreEmpresa'].'&sts='.$row['estatusPatrocinio'].'" class="btn" id="botones">Editar</a></td>';
        $resultado .= "</tr>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }

  function crear_select_anio() {
        $conexion_bd = conectar_bd();  
        $resultado = '<select name="anio" id="anio"><option value="" disabled selected>Selecciona una opción</option>';       
        $consulta = "SELECT year(fechaLarga) as fecha from patrocinador group by year(fechaLarga)";
        $resultados = $conexion_bd->query($consulta);
        while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
            $resultado .= '<option value="'.$row["fecha"].'" ';
            $resultado .= '>'.$row["fecha"].'</option>';
        }
            
        desconectar_bd($conexion_bd);
        $resultado .=  '</select>';
        return $resultado;
  }

  function consultar_patrocinadores_anio($estatus,$orden,$fecha){
     $conexion_bd = conectar_bd();  
    
    $resultado =  '<table class="highlight"><thead><tr><th>Nombre Contacto</th><th>Nombre Empresa</th><th>Teléfono</th><th>Correo</th><th>Descripción Empresa</th><th>Interés</th><th>Patrocinio Obtenido</th><th>Estado actual</th><th>Fecha y Hora</th></tr></thead>';
    
    $consulta = 'Select nombreContacto, nombreEmpresa, telefono, correo, descripcionEmpresa,  descripcionInteres, estatusPatrocinio, patrocinioObtenido, fechaLarga FROM patrocinador';
    $consulta .= ' WHERE estatusPatrocinio="'.$estatus.'"';

    if($fecha != ""){
      $consulta .= ' AND year(fechaLarga) = "'.$fecha.'"';
    }

    if ($orden == 1) {
        $consulta .= " ORDER BY fechaLarga DESC";
    }else{
        $consulta .= " ORDER BY fechaLarga ASC";
    }
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= "<td>".$row['nombreContacto']."</td>"; 
        $resultado .= "<td>".$row['nombreEmpresa']."</td>"; 
        $resultado .= "<td>".$row['telefono']."</td>";
        $resultado .= "<td>".$row['correo']."</td>";
        $resultado .= "<td>".$row['descripcionEmpresa']."</td>";
        $resultado .= "<td>".$row['descripcionInteres']."</td>";
        $resultado .= "<td>".$row['patrocinioObtenido']."</td>";
        $resultado .= "<td>".$row['estatusPatrocinio']."</td>";
        $resultado .= "<td>".$row['fechaLarga']."</td>";
        $resultado .= "</tr>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }


  function consultar_registrosE($estatus,$orden,$fecha){
     $conexion_bd = conectar_bd();  
    
    $resultado =  "<table><thead><tr><th></th><th>Folio</th><th>Nombre Usuario</th><th>Nombre</th><th>Apellido Paterno</th><th>Apellido Materno</th><th>Handicap</th><th>Sexo</th><th>Pago</th><th>Estatus</th><th>Torneo</th><th>Fecha</th><th>Cancelar</th>Cancelar</tr></thead>";
    
    $consulta = 'Select PT.idParticipante, folio, nombreUsuario, nombre , apellidoPaterno, apellidoMaterno, handicap, sexo, estatusPago, estatusInscripcion, idTorneo, fechaLarga  from  participante as P, participante_torneo as PT WHERE PT.idParticipante=P.idParticipante ';
    $consulta .= ' AND estatusInscripcion="'.$estatus.'"';

    if($fecha != ""){
      $consulta .= ' AND year(fechaLarga) = "'.$fecha.'"';
    }
     if ($orden == 1) {
        $consulta .= " ORDER BY folio DESC";
    }else{
        $consulta .= " ORDER BY folio ASC";
    }
      
    $resultados = $conexion_bd->query($consulta); 

    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= '<td><a href="editarRegistro.php?caso_id='.$row['folio'].'"><i class="material-icons">create</i></a></td>'; //Se puede usar el índice de la consulta
        $resultado .= "<td>".$row['folio']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombreUsuario']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['nombre']."</td>";
        $resultado .= "<td>".$row['apellidoPaterno']."</td>";
        $resultado .= "<td>".$row['apellidoMaterno']."</td>";
        $resultado .= "<td>".$row['handicap']."</td>";
        $resultado .= "<td>".$row['sexo']."</td>";
        $resultado .= "<td>".$row['estatusPago']."</td>";
        $resultado .= "<td>".$row['estatusInscripcion']."</td>";
        $resultado .= "<td>".$row['idTorneo']."</td>";
        $resultado .= "<td>".$row['fechaLarga']."</td>";
        $resultado .= '<td><a href="cancelarRegistroUsuarioOrg.php?caso_id='.$row['folio'].'&idP='.$row['idParticipante'].'" class="btn " id="botones"><i class="large material-icons">clear</i></a></td>';
        $resultado .= "</tr>";
    }

   
      
    
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }

  function crear_select_registros() {
        $conexion_bd = conectar_bd();  
        $resultado = '<select name="anioR" id="anioR"><option value="" disabled selected>Selecciona una opción</option>';       
        $consulta = "SELECT year(fechaLarga) as fecha from participante_torneo group by year(fechaLarga)";
        $resultados = $conexion_bd->query($consulta);
        while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
            $resultado .= '<option value="'.$row["fecha"].'" ';
            $resultado .= '>'.$row["fecha"].'</option>';
        }
            
        desconectar_bd($conexion_bd);
        $resultado .=  '</select>';
        return $resultado;
  }
?>