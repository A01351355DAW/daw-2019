<?php
	session_start();
	  if(isset($_SESSION['usuario'])){
	    if($_SESSION['usuario']['nombreRol']!="organizador"){
	      header('Location: pagina-principal.php');
	    }
	  }else{
	      header('Location: InterfazLogin.php');
    }
    require_once("../Models/modelEditarTorneo.php");
	include("header.html");
	include("_formTorneo.html");
	echo mostrarTorneos();

	include("footer.html");
?>