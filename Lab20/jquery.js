$(function(){
	$("#nombres").prepend("Estos son las sugerencias de nombres: ");
	var title = $("h3").html("Este es un buscador de Avengers");
	$("#p1").text("Para algunas funciones de jQuery, existen algunas otras alternativas para solucionar de manera similar el problema, por ejemplo: Para seleccionar algo, en lugar de usar jQuery, se puede utilizar el API HTML DOM y usar el document.querySelector().Los request Ajax HTTP pueden ser ejecutados con el API de JS “fetch”. Las animaciones, en vez de utilizar el método “.animate”, pueden ser ejecutadas mediante las animaciones de CSS");
	$("#p2").text("Es el objeto que representa que una operación asíncrona ha terminado o ha fracasado. Puede encontrarse en estado pendiente, cumplida o rechazada.");
	$("#p3").text("La palabra async antes de una función significa que dicha función siempre regresa una promesa. La función await obliga a JS a esperar a que la promesa se solucione y devuelva su resultado.");
});

