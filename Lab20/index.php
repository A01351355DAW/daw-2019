<?php 
  session_start();
  require_once("model.php");  

  $titulo = "Buscador";
  include("_header.html");  

  include("_form.html"); 

  include("_btn_agregar.html");
  
  if (isset($_POST["heroe"])) {
      $heroe = htmlspecialchars($_POST["heroe"]);
  } else {
      $heroe = "";
  }

if (isset($_POST["pelicula"])) {
      $pelicula = htmlspecialchars($_POST["pelicula"]);
  } else {
      $pelicula = "";
  }

  echo consultar_casos($heroe,$pelicula);

  include("_btn_agregar.html");

  include("_footer.html"); 
?>