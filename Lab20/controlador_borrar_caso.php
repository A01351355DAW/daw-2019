<?php
  session_start();
  require_once("model.php");  

  $_POST["caso"] = htmlspecialchars($_POST["caso"]);

  if(isset($_POST["caso"])) {
      if (borrar_registro_id($_POST["caso"])) {
          $_SESSION["mensaje"] = "Se borró el héroe";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al borrar el héroe";
      }
  }
  header("location:index.php");
?>