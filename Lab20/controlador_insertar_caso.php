<?php 
	session_start();
  require_once("model.php");  

  $_POST["heroe"] = htmlspecialchars($_POST["heroe"]);
  $_POST["pelicula"] = htmlspecialchars($_POST["pelicula"]);

  if(isset($_POST["heroe"])) {
      insertar_caso($_POST["heroe"]);
      if (insertar_caso($_POST["heroe"])) {
          $_SESSION["mensaje"] = "Se registró el héroe";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al registrar al héroe";
      }
  }

  if(isset($_POST["pelicula"])) {
    $id = consultar_idCaso();
    if (insertar_completo($id,$_POST["pelicula"])) {
          $_SESSION["mensaje"] = "Se registró el caso";
    } else {
          $_SESSION["warning"] = "Ocurrió un error al registrar el caso";
    }
  }

  header("location:index.php");
?>