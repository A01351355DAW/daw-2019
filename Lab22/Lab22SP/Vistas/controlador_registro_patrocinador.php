<?php
    session_start();
    require_once("modelRegistro.php");
    $nombre = htmlspecialchars($_POST["nombre"]);
    $nombreE = htmlspecialchars($_POST["nombreEmpresa"]);
    $desc = htmlspecialchars($_POST["descripcion"]);
    $telefono = htmlspecialchars($_POST["telefono"]);
    $email = htmlspecialchars($_POST["email"]);
    $razonPatrocinio = htmlspecialchars($_POST["Razonpatrocinio"]);
    echo registrarPatrocinador($nombre,$nombreE,$desc,$telefono,$email,$razonPatrocinio);
?>