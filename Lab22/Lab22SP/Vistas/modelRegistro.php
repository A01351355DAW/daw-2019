<?php

function conectar_bd() {
  $conexion_bd = mysqli_connect("localhost","root","","torneogolf");
  if ($conexion_bd == NULL) {
      die("No se pudo conectar con la base de datos");
  }
  return $conexion_bd;
}
  

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }

  function insertarCuenta($username, $password, $email, $idRol){
    $conexion_bd = conectar_bd();

    $dml = 'INSERT INTO cuenta (nombreUsuario,Password,correoElectronico,IdRol) VALUES (?,?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
        //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("sssi", $username,$password,$email,$idRol)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }


function idOrg(){
    $conexion_bd = conectar_bd();  
      
    $consulta = "SELECT idOrganizador FROM organizador ORDER BY idOrganizador DESC LIMIT 1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["idOrganizador"];
    }
        
    desconectar_bd($conexion_bd);
    return 0;
  }


  function insertarOrganizador($nombre,$apellidoP,$apellidoM,$telefono,$email,$username, $password){
    $conexion_bd = conectar_bd();
    //Primero Crea una cuenta para el organizador
    if(!insertarCuenta($username,$password,$email,2)){
      die("Error al tartar de registrar la cuenta");
      return 0;
    }
    //Encuentra el ultimo id del organizador y le suma 1 para crear el nuevo id
    $id=idOrg();
    $id=$id+1;
    $des=0;

    $dml = 'INSERT INTO organizador (nombreUsuario,idOrganizador,nombre,apellidoPaterno,apellidoMaterno,desactivado) VALUES (?,?,?,?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    if (!$statement->bind_param("sisssi", $username,$id,$nombre,$apellidoP,$apellidoM,$des)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }


  function borrarCuenta($username){
    $conexion_bd = conectar_bd();
    $dml = "DELETE FROM cuenta WHERE nombreUsuario='".$username."'";
    $resultado=$conexion_bd->query($dml);
    desconectar_bd($conexion_bd);  
    return $resultado;
  }

  function revisaOrgTorneo($username){
    $conexion_bd = conectar_bd();
    $dml = "SELECT nombreUsuario FROM organizador_torneo WHERE nombreUsuario='".$username."'";
    $resultados=$conexion_bd->query($dml);
    $resultado="";
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
      $resultado.=$row['nombreUsuario'];
    }
    desconectar_bd($conexion_bd);  
    return $resultado;
  }    


  function borrarOrganizador($id){
    $conexion_bd = conectar_bd();
    $desactivado = revisaOrgTorneo($id);

    if($desactivado!=""){
      $dml="UPDATE organizador SET desactivado = 1 WHERE nombreUsuario='".$id."'";
      if(! $conexion_bd->query($dml)){
      return 0;
      }else{
        return 1;
      }
    }

    $dml="DELETE FROM organizador WHERE nombreUsuario='".$id."'";
    if(! $conexion_bd->query($dml)){
      return 0;
    }
    $exito=borrarCuenta($id);
    desconectar_bd($conexion_bd);
    return $exito;
  }
  function idParticipante(){
    $conexion_bd = conectar_bd();  
      
    $consulta = "SELECT idParticipante FROM participante ORDER BY idParticipante DESC LIMIT 1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["idParticipante"];
    }
        
    desconectar_bd($conexion_bd);
    return 0;
  }


function insertarParticipante($username,$nombre,$apellidoP,$apellidoM,$handicap,$sexo,$edad,$telefono,$email){
    $conexion_bd = conectar_bd();
    $a=NULL;
    $dml= 'INSERT INTO participante(idParticipante,nombreUsuario,nombre,apellidoPaterno,apellidoMaterno,handicap,sexo,edad,telefono,correo) VALUES(?,?,?,?,?,?,?,?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    if (!$statement->bind_param("issssdsiss",$a,$username,$nombre,$apellidoP,$apellidoM,$handicap,$sexo,$edad,$telefono,$email)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    desconectar_bd($conexion_bd);
      return 1;
  }

function insertarInscripcion($nombre,$apellidoP,$apellidoM,$sexo,$edad,$telefono,$email,$handicap,$indicacionEspecial,
$metodopago,$facturacion,$autorizarFotos,$factRazonSocial,
$factRFC,$factDireccionFiscal,$factIndicacionEsp,
$urlRecibo,$indicacionEspecialPago,$username){
      
      $conexion_bd = conectar_bd();
      $estatusPago='Pendiente';
      $estatusInscripcion='Pendiente';
      $a=NULL;
      $idTorneo=1;
      
      $asistenciaTorneo="No";
      if(! insertarParticipante($username,$nombre,$apellidoP,$apellidoM,$handicap,$sexo,$edad,$telefono,$email)){
        die("Error al tartar de registrar participante");
          return 0;
      }
      $idParticipante=idParticipante();
      
      $dml = 'INSERT INTO participante_torneo (folio,idParticipante,idTorneo, indicacionEspecial, metodoPago, facturacion, autorizarFotos, RFC, razonSocial, domicilioFiscal, comentariosFacturacion, idFotoRecibo, indicacionPagoEspecial, estatusInscripcion, estatusPago, asistenciaAlTorneo) 
      VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';
      if ( !($statement = $conexion_bd->prepare($dml)) ) {
          die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
          return 0;
      }
      
      if (!$statement->bind_param("iiisssssssssssss",$a,$idParticipante,$idTorneo,$indicacionEspecial,$metodopago,$facturacion,$autorizarFotos,$factRFC,$factRazonSocial,$factDireccionFiscal,$factIndicacionEsp,$urlRecibo,$indicacionEspecialPago,$estatusInscripcion,$estatusPago,$asistenciaTorneo)) {
          die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }
      if (!$statement->execute()) {
        ("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
          return 0;
      }

      desconectar_bd($conexion_bd);
      return 1;
    
  }
  

    function idTorneo(){
      $conexion_bd = conectar_bd();  
        
      $consulta = "CALL consultaidT()";
      $resultados = $conexion_bd->query($consulta);
      while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
          desconectar_bd($conexion_bd);
          return $row["iDTorneo"];
      }
          
      desconectar_bd($conexion_bd);
      return 0;
  }

  function insertarTorneo($fecha,$horaR,$horaI,$lugar,$costo,$cierre){
    $conexion_bd = conectar_bd();
    //var_dump($fecha);
    //var_dump($cierre);
    //Encuentra el ultimo id del organizador y le suma 1 para crear el nuevo id
    $id=idTorneo();
    $id=$id+1;


    $dml = "CALL crearT(?,?,?,?,?,?,?)";

    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    if (!$statement->bind_param("issssis",$id,$fecha,$horaR, $horaI, $lugar, $costo, $cierre)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    //var_dump($statement);
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }

  function registrarPatrocinador($nombre,$nombreE,$desc,$telefono,$email,$razonPatrocinio){
   $conexion_bd = conectar_bd();
   $dml = 'INSERT INTO patrocinador(nombreContacto,nombreEmpresa,telefono,correo,descripcionEmpresa,descripcionInteres,estatusPatrocinio) VALUES (?,?,?,?,?,?,?)';
   $estatus="Pendiente";
   if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    if (!$statement->bind_param("sssssss",$nombre,$nombreE,$telefono,$email,$desc,$razonPatrocinio,$estatus)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
   desconectar_bd($conexion_bd);
   return 1;
  }


  function editarPatrocinador($fecha,$patrocinio,$estatus){
    $conexion_bd = conectar_bd();
    $dml ='UPDATE patrocinador SET estatusPatrocinio="'.$estatus.'", patrocinioObtenido="'.$patrocinio.'" where fechaLarga="'.$fecha.'"';
    if(!$conexion_bd->query($dml)){
      desconectar_bd($conexion_bd);
      return 0;
    }
    desconectar_bd($conexion_bd);
    return 1;
  }
  
?>

