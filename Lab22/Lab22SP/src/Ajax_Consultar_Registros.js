function consultaRegistrosE(){
	let opcion = document.getElementById("mostrar");
    let opcionVal = opcion.options[opcion.selectedIndex].value;
    let opcion2 = document.getElementById("registros");
    let opcionVal2 = opcion2.options[opcion2.selectedIndex].value;
    let status="";
    let orden="";
    if(opcionVal!="NA" && opcionVal2!="NA"){
        if(opcionVal2==1){
        	status="Pendiente";
        }else {
        	status="Inscrito";
        }
        if(opcionVal==1){
        	orden="1";
        }else{
        	orden="2";
        }
    }else{
        let error=document.getElementById('MRR');
        error.innerHTML="<h5>Por favor selecciona el estatus y el orden en el cual buscar</h5>";
        error.style.display="block";
        return 0;
    }
    $.post("controlador_consulta_registros.php", {
        estatus: status,
        ordenB: orden
    }).done(function (data) {
        let consulta=document.getElementById('Registros');
        consulta.style.display="block";
        let select=document.getElementById('SelectR');
        select.style.display="none";
        let consulta2=document.getElementById('ConsultaR');
        consulta2.innerHTML=data;
        let botonRegresar=document.getElementById('RegresarBuscar');
        botonRegresar.style.display="block";
       });
}

function regresar(){
    let consulta=document.getElementById('Registros');
    consulta.style.display="none";
    let consulta2=document.getElementById('Registros2');
    consulta2.style.display="none";
    let select=document.getElementById('SelectR');
    select.style.display="block";
    let botonRegresar=document.getElementById('RegresarBuscar');
    botonRegresar.style.display="none";
    let error=document.getElementById('MRR');
    error.style.display="none";
}

var habilitadaSeleccionarM=document.getElementById("botonB");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = consultaRegistrosE;
    var botonR=document.getElementById("RegresarBuscar");
    botonR.onclick = regresar;
}

function consultaTodo(){
    
    $.post("controlador_consulta_registros.php", {
        
    }).done(function (data) {
        let consulta=document.getElementById('Registros2');
        consulta.style.display="block";
        let select=document.getElementById('SelectR');
        select.style.display="none";
        let consulta2=document.getElementById('ConsultaT');
        consulta2.innerHTML=data;
        let botonRegresar=document.getElementById('RegresarBuscar');
        botonRegresar.style.display="block";
       });
}

var habilitadaSeleccionarM=document.getElementById("botonT");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = consultaTodo;
    var botonR=document.getElementById("RegresarBuscar");
    botonR.onclick = regresar;
}