function registraCambios(){
	let opcionPatrocinio = document.getElementById("estatus_Patrocinio");
    let opcionVal = opcionPatrocinio.options[opcionPatrocinio.selectedIndex].value;
    let pO = $("#PatrocinioObt").val();
    let status="";
    if(opcionVal!="NA"){
        if(opcionVal==1){
        	status="Pendiente";
        }else if(opcionVal==2){
        	status="Aceptado";
        }else{
        	status="Rechazado";
        }
    }else{
        let error=document.getElementById('encabezadoMensajeRP');
        error.innerHTML="<h5>Por favor selecciona el nuevo estatus del patrocinador</h5>";
        error.style.display="block";
        return 0;
    }
    let circulo=document.getElementById("circuloCarga_RP");
    circulo.style.display="block";
    let info=document.getElementById('divRP');
            info.style.display="none";
    $.post("controlador_cambio_patrocinios.php", {
        estatus: status,
        patrocinio: pO
    }).done(function (data) {
        if(data==1){
            let info=document.getElementById('divRP');
            info.style.display="none";
            let regresa=document.getElementById('RegresarRP');
            regresa.style.display="block";
            let secMensaje=document.getElementById('secMensajeRP');
            secMensaje.style.display="block";
            let consulta2=document.getElementById('MensajeRP');
            consulta2.innerHTML="Se cambió el estatus del patrocinio exitosamente";
            let circulo=document.getElementById("circuloCarga_RP");
            circulo.style.display="none";
        }else{
            let info=document.getElementById('divRP');
            info.style.display="none";
            let regresa=document.getElementById('RegresarRP');
            regresa.style.display="block";
            let secMensaje=document.getElementById('secMensajeRP');
            secMensaje.style.display="block";
            let consulta2=document.getElementById('MensajeRP');
            consulta2.innerHTML="No se pudo cambiar el estatus del patrocinio exitosamente";
            let icono=document.getElementById('iconoRespuestaRP');
            secMensaje.style.display="alert";
            let circulo=document.getElementById("circuloCarga_RP");
            circulo.style.display="none";
        }
       });
}


var habilitadaSeleccionarM=document.getElementById("BRP");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = registraCambios;
}