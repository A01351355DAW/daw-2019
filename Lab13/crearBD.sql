DROP TABLE IF EXISTS Materiales;

CREATE TABLE Materiales
(
  Clave numeric(5) not null,
  Descripcion varchar(50),
  Costo numeric (8,2)
);

DROP TABLE IF EXISTS Proveedores;

CREATE TABLE Proveedores
(
  RFC char(13) not null,
  RazonSocial varchar(50)
);

DROP TABLE IF EXISTS Proyectos;

CREATE TABLE Proyectos
(
  Numero numeric(5) not null,
  Denominacion varchar(50)
);
DROP TABLE IF EXISTS Entregan;

CREATE TABLE Entregan
(
  Clave numeric(5) not null,
  RFC char(13) not null,
  Numero numeric(5) not null,
  Fecha DateTime not null,
  Cantidad numeric (8,2)
);

LOAD DATA INFILE 'C:\Users\Emilio\Documents\1.- TEC\Profesional\4to Semestre\DAW\daw-2019\Lab11\proveedores.txt'
INTO TABLE bdfj20.proveedores
fields terminated BY ','
lines terminated by '\r\n';


LOAD DATA INFILE 'C:\Users\Emilio\Documents\1.- TEC\Profesional\4to Semestre\DAW\daw-2019\Lab11\entregan.txt'
INTO TABLE bdfj20.entregan
FIELDS TERMINATED BY ','
lines terminated by '\n'
(Clave,RFC,Numero, @Fecha,Cantidad)
SET Fecha = STR_TO_DATE(@Fecha, '%d/%m/%Y');

LOAD DATA INFILE 'C:\Users\Emilio\Documents\1.- TEC\Profesional\4to Semestre\DAW\daw-2019\Lab11\materiales.txt'
INTO TABLE bdfj20.materiales
FIELDS TERMINATED BY ','
lines terminated by '\n';


LOAD DATA INFILE 'C:\Users\Emilio\Documents\1.- TEC\Profesional\4to Semestre\DAW\daw-2019\Lab11\proyectos.txt'
INTO TABLE bdfj20.proyectos
FIELDS TERMINATED BY ','
lines terminated by '\n';