function exponentes(){
  var numero = prompt("Inserte un número por favor");
  document.write("<table border='1'><tr><td>Número Original</td><td>Número Cuadrado</td><td>Número al Cubo</td></tr>");
  for(let i = 1; i <= numero; i++){
    document.write("<tr><td>" + i + "</td><td>" + i * i + "</td><td>" + i * i * i + "</td></tr>");
  }
  document.write("</table>");
}

function suma(){
        numero1= Math.floor(Math.random()*100);
        numero2= Math.floor(Math.random()*100);
        var fecha = new Date();
        resultado = prompt("Contesta "+ numero1 +" + "+ numero2 +" =");
        tiempo = ( new Date() - fecha) / 1000;
        if(resultado == (numero1 + numero2))
        {
            alert("Correcto!" + "\n" + "Tiempo en responder: " + tiempo +"s");
        }
        else{
            alert("Incorreto" + "\n" + "Tiempo en responder: " + tiempo +"s");
        }
  }
  
 function contador(){
    var salir = 'n';
    var numeros = [];
    while (salir == 'n') {
        numero = prompt('Inserte un número')
        if (numero != null) {
            numeros.push(parseInt(numero));
        }
        var insertar_mas = confirm('Insertar más números?')
        if (insertar_mas == false) {
            salir = 's';
        }
    }
    pos = 0, neg = 0, ceros = 0;
    for(i = 0; i <= numeros.length; i++){
        if(numeros[i] == 0){
            ceros++;
        }else if(numeros[i] < 0){
            neg++;
        }else{
            pos++;
        }
    }
    document.write("En los números insertados hay:<br> "+ ceros + " : ceros<br> "+ pos +" : números positivos <br>"
    + neg +" : números negativos");
  }

  function promedios(){
  let matriz = [[5,10],[7,5],[9,2]];
  let arreglo = [];
  let aux=[];
  let promedio = 0, contador = 0;
  for(let i = 0; i < matriz.length; i++){
    aux = matriz[i];
    for(let j = 0; j < aux.length; j++){
      promedio = promedio + aux[j];
      contador++;
    }
    promedio = (promedio/contador);
    arreglo.push(promedio);
    document.write("El promedio es: " + promedio + "<br>");
    promedio = 0;
    contador = 0;
  }
}

  function inverso(){
        numero = prompt("Inserte número que se va a invertir");
        aux = numero;
        let inverso = 0;
        while(numero > 0){
            inverso = inverso * 10;
            inverso = inverso + (numero % 10);
            numero = parseInt(numero / 10);
        }
        document.writeln("Inverso de "+ aux +" es: "+ inverso);
        
    }


    function openWin() {
  var myWindow = window.open("", "myWindow", "width=580, height=190");
  myWindow.document.write("<h3>Referencias <br>https://www.gestiopolis.com/diferencias-java-javascript/<br>https://tutobasico.com/date-javascript/<br>https://tutobasico.com/metodos-arrays-javascript/<br>https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Sentencias/let<br>https://www.w3schools.com/js/js_scope.asp<br>https://www.geeksforgeeks.org/javascript-match/<br></h3> ");
  setTimeout(function(){ myWindow.close() }, 10000);
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
}

