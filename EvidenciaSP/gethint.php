<?php
// Array with names
$a[] = "Tony Stark";
$a[] = "Steve Rogers";
$a[] = "Clint Barton";
$a[] = "Peter Parker";
$a[] = "Natasha Romanoff";
$a[] = "Carol Danvers";
$a[] = "Thor Odinson";
$a[] = "Bruce Banner";
$a[] = "Sam Wilson";
$a[] = "Wanda Maximoff";
$a[] = "Drax";
$a[] = "Peter Quill";
$a[] = "Gamora";
$a[] = "Groot";
$a[] = "Rocket";
$a[] = "Mantis";
$a[] = "Stephen Strange";
$a[] = "Scott Lang";
$a[] = "James Barnes";
$a[] = "T'Challa";
$a[] = "Vision";
$a[] = "Nick Fury";
$a[] = "James Rhodes";
$a[] = "Stan Lee";
$a[] = "Nebula";
$a[] = "Loki Odinson";
$a[] = "Emilio Aguilera";
$a[] = "Ivan Diaz";
$a[] = "Eric Torres";
$a[] = "Nahim Medellín";

// get the q parameter from URL
$q = $_REQUEST["q"];

$hint = "";

// lookup all hints from array if $q is different from ""
if ($q !== "") {
  $q = strtolower($q);
  $len=strlen($q);
  foreach($a as $name) {
    if (stristr($q, substr($name, 0, $len))) {
      if ($hint === "") {
        $hint = $name;
      } else {
        $hint .= ", $name";
      }
    }
  }
}

// Output "no suggestion" if no hint was found or output correct values
echo $hint === "" ? "no suggestion" : $hint;
?>