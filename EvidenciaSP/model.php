<?php 
  //función para conectarnos a la BD
  function conectar_bd() {
      $conexion_bd = mysqli_connect("localhost","root","","avengers");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }


  function consultar() {
    $conexion_bd = conectar_bd();  
    
    $resultado =  "<table><thead><tr><th>Num</th><th>Héroe</th><th>Película</th><th>Fecha y hora</th></tr></thead>";
    
    $consulta = "CALL consultaHeroes()";
    
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= "<td>".$row['caso_id']."</td>"; //Se puede usar el índice de la consulta
        $resultado .= "<td>".$row['L_nombre']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['E_nombre']."</td>";
        $resultado .= "<td>".$row['T_created_at']."</td>";
        $resultado .= "</tr>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }
  //Consulta los casos de coronavirus
  //@param $heroe: El heroe de donde proviene el caso
  //@param $pelicula: El pelicula de la infección del caso
  function consultar_casos($heroe="", $pelicula="") {
    $conexion_bd = conectar_bd();  
    
    $resultado =  "<table><thead><tr><th>Num</th><th>Héroe</th><th>Película</th><th>Fecha y hora</th></tr></thead>";
    
    $consulta = 'Select caso_id, L.nombre as L_nombre, E.nombre as E_nombre, T.created_at as T_created_at From pelicula as E, Tiene as T, heroe as L, caso as C WHERE E.id = T.pelicula_id AND C.id = T.caso_id AND C.heroe_id = L.id';
    if ($heroe != "") {
        $consulta .= " AND heroe_id=".$heroe;
    }
    if ($pelicula != "") {
        $consulta .= " AND pelicula_id=".$pelicula;
    }
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= '<td><a href="controlador_editar.php?caso_id='.$row['caso_id'].'">'.$row['caso_id']."</a></td>"; //Se puede usar el índice de la consulta
        $resultado .= "<td>".$row['L_nombre']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['E_nombre']."</td>";
        $resultado .= "<td>".$row['T_created_at']."</td>";
        $resultado .= "</tr>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }

  //Crea un select con los datos de una consulta
  //@param $id: Campo en una tabla que contiene el id
  //@param $columna_descripcion: Columna de una tabla con una descripción
  //@param $tabla: La tabla a consultar en la bd
  function crear_select($id, $columna_descripcion, $tabla, $seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '<div class="input-field"><select name="'.$tabla.'"><option value="" disabled selected>Selecciona una opción</option>';
            
    $consulta = "SELECT $id, $columna_descripcion FROM $tabla ORDER BY $tabla.`id` ASC";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<option value="'.$row["$id"].'" ';
        if($seleccion == $row["$id"]) {
            $resultado .= 'selected';
        }
        $resultado .= '>'.$row["$columna_descripcion"].'</option>';
    }
        
    desconectar_bd($conexion_bd);
    $resultado .=  '</select><label>'.$tabla.'...</label></div>';
    return $resultado;
  }

  //función para insertar un registro de caso de coronavirus
  //@param heroe_id: id de la tabla heroe en la base de datos
 function insertar_caso($heroe_id) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = "CALL  insertarHeroe(?)";
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i", $heroe_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }

    desconectar_bd($conexion_bd);
    return 1;
  }

  //Insterta el caso en la tabla tiene
  function insertar_completo($caso_id,$pelicula_id) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'CALL insertarTodo(?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss",$caso_id,$pelicula_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }

  //Funcion que devuelve id del caso
  function consultar_idCaso() {
    $conexion_bd = conectar_bd();  
    
    $resultado = "";
    
    $consulta = 'Select id From caso ORDER BY `caso`.`id` ASC';
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado = $row['id'];
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
    return $resultado;
  }

  //Borrara un registro basandose en su id
  function borrar_registro_id($id){
    $conexion_bd = conectar_bd();

    $consulta = "CALL borrarHeroe(?)";

    if ( !($statement = $conexion_bd->prepare($consulta)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i", $id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    //$resultado = mysqli_query($conexion_bd,$consulta);

    $consulta = "CALL borrarID(?)";
    if ( !($statement = $conexion_bd->prepare($consulta)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i", $id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    //$resultado2 = mysqli_query($conexion_bd,$consulta);
    
    desconectar_bd($conexion_bd); 

    return 1;
  }

  //función para editar un registro de caso de coronavirus
  //@param caso_id: id del caso que se va a editar
  //@param heroe_id: id de la tabla heroe en la base de datos
  function editar_caso($caso_id, $heroe_id) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = "CALL editarHeroe(?,?)";
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ii", $heroe_id, $caso_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    desconectar_bd($conexion_bd);
      return 1;
  }

  //Consultar el id del heroe a partir del id de un caso
  //@param $caso_id: El id del caso
  function recuperar_heroe($caso_id) {
    $conexion_bd = conectar_bd();  
      
    $consulta = "SELECT heroe_id FROM caso WHERE id=$caso_id";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["heroe_id"];
    }
        
    desconectar_bd($conexion_bd);
    return 0;
  }    
?>