<?php
  session_start();
  require_once("model.php");  

  $_POST["heroe"] = htmlspecialchars($_POST["heroe"]);
  $_POST["caso_id"] = htmlspecialchars($_POST["caso_id"]); 

  if(isset($_POST["heroe"])) {
      if (editar_caso($_POST["caso_id"], $_POST["heroe"])) {
          $_SESSION["mensaje"] = "Se editó el caso";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al editar el caso";
      }
  }

  header("location:index.php");
?>