<?php 
  include("header.html"); 
  $arr = array(1,5,8,2,5,1,6);
  $arr2 = array(13,18,8,2,50,30,12);

  echo "<h1>Función Promedio</h1>";
  function promedio(array $array){
    $contador = count($array);
    $suma = 0;
    echo "<p>El promedio de ";
    for($i = 0; $i <= $contador-1; $i++){
      $suma = $suma + $array[$i];
      echo "$array[$i] ";
    }
    $promedio = $suma / $contador;
    echo " es igual a $promedio</p> ";
    echo PHP_EOL;
  }
  promedio($arr);
  promedio($arr2);

  echo "<h1>Función Mediana</h1>";
  function mediana(array $array){
    $tamaño = count($array);
    echo "<p>La mediana de: ";
    for($i = 0; $i <= $tamaño-1; $i++){
      echo "$array[$i] ";
    }
    echo "</br>acomodado";
    sort($array);
    foreach ($array as $key => $val) {
      echo " $val ";
    }
    $operacion = ($tamaño)/2;
    $mediana = $array[$operacion];
    echo "es: $mediana</p>";
  }
  mediana($arr);
  mediana($arr2);

  echo "<h1>Función Lista</h1>";
  function lista(array $array){
    $tamaño = count($array);
    for($i = 0; $i <= $tamaño-1; $i++){
      echo "<strong>$array[$i] </strong>";
    }
    echo "<ul><li>";
    promedio($array);
    echo "</li></ul>";
    echo "<ul><li>";
    mediana($array);
    echo "</li></ul>";
    echo "<ul><li>Ordenado ascendente ";
    sort($array);
    foreach ($array as $key => $val) {
      echo " $val ";
    }
    echo "</li></ul>";
    echo "<ul><li>Ordenado descendente ";
    rsort($array);
    foreach ($array as $key => $val) {
      echo " $val ";
    }
    echo "</li></ul>";
  }
  lista($arr);
  lista($arr2);

  echo "<h1>Función Tabla</h1>";
  function tabla($num){
    
    echo "<table border='1'><tr><td>Número Original</td><td>Número Cuadrado</td><td>Número al Cubo</td></tr>";
    for($i = 1; $i <= $num; $i++){
      $cuadrado = $i * $i;
      $cubo = $i * $i *$i;
      echo "<tr><td> $i </td><td> $cuadrado </td><td> $cubo </td></tr>";
    }
    echo "</table>";
  }
  tabla(11);
  tabla(15);

  echo "<h1>Función Operaciones</h1>";
  function operaciones($num1, $num2){
    echo "<p>Numeros: $num1 , $num2</p>";
    $suma = $num1 + $num2;
    echo "<ul>Suma: $num1 + $num2 = $suma</ul>";
    $resta = $num1 - $num2;
    echo "<ul>Resta: $num1 - $num2 = $resta</ul>";
    $mult = $num1 * $num2;
    echo "<ul>Multiplicación: $num1 * $num2 = $mult</ul>";
    $div = $num1 / $num2;
    echo "<ul>División: $num1 / $num2 = $div</ul>";
    $mod = $num1 % $num2;
    echo "<ul>Modulo: $num1 % $num2 = $mod</ul>";
  }
  operaciones(8,5);
  operaciones(20,4);
  include("preguntas.html");
  include("footer.html");
?>