<?php

function conectar_bd() {
    $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_torneo","torneo","dawbdorg_torneogolf");
    if ($conexion_bd == NULL) {
        die("No se pudo conectar con la base de datos");
    }
    return $conexion_bd;
}
function desconectar_bd($conexion_bd) {
    mysqli_close($conexion_bd);
}
function registrarEquipo($arrayJugadores,$hoyoInicial,$tamañoEquipo){
    $conexion_bd = conectar_bd();
    if(! crearEquipo($hoyoInicial,$tamañoEquipo)){
        die("Error al tartar de registrar participante");
          return "Error en creando equipo";
      }
    $idEquipo=idEquipo();
    $idTorneo=idTorneo();
    for ($i=0; $i <$tamañoEquipo; $i++) { 
        $dml = 'INSERT INTO participante_equipo_torneo(idParticipante,idTorneo,idEquipo) VALUES (?,?,?)';
        if ( !($statement = $conexion_bd->prepare($dml)) ) {
            die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
            return 0;
        }
        if (!$statement->bind_param("iii",$arrayJugadores[$i],$idTorneo,$idEquipo)) {
            die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }
        if (!$statement->execute()) {
          die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
            return 0;
        }  
    }
    desconectar_bd($conexion_bd);
    return 1;
  }

function crearEquipo($hoyoInicial,$tamaño){
    $conexion_bd = conectar_bd();
    $a=null;
    $dml = 'INSERT INTO equipo(idEquipo,numParticipantes,hoyoInicial) VALUES (?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
    }
    if (!$statement->bind_param("iii",$a,$tamaño,$hoyoInicial)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
    if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
    }
    desconectar_bd($conexion_bd);
    return 1;
}
function idTorneo(){
    $conexion_bd = conectar_bd();
    $consulta = "Select idTorneo from torneo where activo=1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["idTorneo"];
    }
    desconectar_bd($conexion_bd);
    return 0;
  }

function idEquipo(){
    $conexion_bd = conectar_bd();  
    $consulta = "SELECT idEquipo FROM equipo ORDER BY idEquipo DESC LIMIT 1";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        desconectar_bd($conexion_bd);
        return $row["idEquipo"];
    }
    desconectar_bd($conexion_bd);
    return 0;
  }
  
?>