
function cancelarRegistroUsuarioOrg() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    let circulo=document.getElementById("circuloCarga_CancelarUsuarioOrg");
    circulo.style.display="block";
    let forma=document.getElementById('formaCancelarUsuarioOrg');
    forma.style.display="none";
    $.post("../Controladores/controlador_cancelar_usuario.php", {
        
    }).done(function (data) {
        if(data==1){
            let forma=document.getElementById('formaCancelarUsuarioOrg');
            forma.style.display="none";
            let ajaxResponse=document.getElementById('mensajeEditarUsuarioOrg');
            let titulo=document.getElementById('confirmacionCancelarUsuarioOrg');
            let circulo=document.getElementById("circuloCarga_CancelarUsuarioOrg");
            circulo.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se canceló exitosamente el registro";
        }else{
            let ajaxResponse=document.getElementById('mensajeEditarUsuarioOrg');
            let titulo=document.getElementById('confirmacionCancelarUsuarioOrg');
            let forma=document.getElementById('formaCancelarUsuarioOrg');
            let circulo=document.getElementById("circuloCarga_CancelarUsuarioOrg");
            circulo.style.display="none";
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudo cancelar exitosamente el registro";
            
        }
    });
}




var habilitadaSeleccionarM=document.getElementById("botonCancelarUsuarioOrg");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = cancelarRegistroUsuarioOrg;
}