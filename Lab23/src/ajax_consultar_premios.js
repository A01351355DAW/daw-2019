//Función que detonará la petición asíncrona como se hace ahora con la librería jquery
function buscarPremios() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    $.post("../Controladores/controlador_consulta_premios.php", {
        torneo: $("#torneo").val(),
        tipoPremio: $("#tipoPremio").val()
    }).done(function (data) {
        $("#resultados_consulta_premios").html(data);
    });
}

async function buscar_vanilla() {
    //Para poder pasar parámetros
    let parametros = new FormData();
    parametros.append("torneo", document.getElementById("torneo").value);
    parametros.append("tipoPremio", document.getElementById("tipoPremio").value);
    try {
        //await sirve para indicar que en este punto se espera una petición asíncrona
        //fetch es la función que hace la petición asíncrona
        await fetch('../Controladores/controlador_consulta_premios.php', {
            method: 'POST', //El método GET de fetch no permite parámetros
            body: parametros
                //.then se ejecuta cuando concluye la petición asíncrona, pero esto también genera una promesa que se ejecuta de manera asíncrona
        }).then(function (response) {
            return response.text();
            //Este segundo then nos permite recuperar el contenido de la respuesta cuando se termina la promesa anterior
        }).then(function (data) { //
            document.getElementById("resultados_consulta_premios").innerHTML = data;
        });
        //El uso de async y await permite que atrapar un error en la comunicación
    } catch (e) {
        console.log(e);
        document.getElementById("resultados_consulta_premios").innerHTML = "Error en la comunicación con el servidor";
    }
}

var habilitadaCP=document.getElementById("buscarPremios")
if(habilitadaCP!=null){
    habilitadaCP.onclick = buscarPremios;
}


function agregarPremio() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    /*if(!validar()){
        return 0;
    } */
    let circulo=document.getElementById("circuloCarga_agregarPremio");
    circulo.style.display="block";
    let forma=document.getElementById('formaAgregarPremio');
    forma.style.display="none";
    $.post("../Controladores/controlador_agregar_premio.php", {
        torneoAgregar: $("#torneo").val(),
        tipoPremioAgregar: $("#tipoPremio").val(),
        descripcionAgregar: $("#descripcionAgregar").val(),
        posicionAgregar: $("#posicionAgregar").val(),
        hoyoAgregar: $("#hoyoAgregar").val()
    }).done(function (data) {
        if(data==1){
            let forma=document.getElementById('formaAgregarPremio');
            forma.style.display="none";
            let ajaxResponse=document.getElementById('mensajeAgregarPremio');
            let titulo=document.getElementById('tituloAgregarPremio');
            let circulo=document.getElementById("circuloCarga_agregarPremio");
            circulo.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML = "Se registró exitosamente el nuevo premio";
        }else{
            let ajaxResponse=document.getElementById('mensajeAgregarPremio');
            let titulo=document.getElementById('tituloAgregarPremio');
            let forma=document.getElementById('formaAgregarPremio');
            let circulo=document.getElementById("circuloCarga_agregarPremio");
            circulo.style.display="none";
            forma.style.display="none";
            ajaxResponse.style.display="block";
            titulo.innerHTML="No se pudo registrar exitosamente el premio. Intente más tarde";
            let iconoRespuesta=document.getElementById('iconoRespuesta');
            iconoRespuesta.innerHTML="error";
            
        }
    });
}


var habilitadaRegistrarO=document.getElementById("agregarPremio")
if(habilitadaRegistrarO!=null){
    habilitadaRegistrarO.onclick = agregarPremio;
}
