<?php
    session_start();
    require_once("../Models/modelRegistro.php");
    

// Checks if form has been submitted
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	    function post_captcha($user_response) {
	        $fields_string = '';
	        $fields = array(
	            'secret' => '6LdxBQEVAAAAAOWHCyjP_bThCKwZjPPCjx1rC3jQ',
	            'response' => $user_response
	        );
	        foreach($fields as $key=>$value)
	        $fields_string .= $key . '=' . $value . '&';
	        $fields_string = rtrim($fields_string, '&');

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
	        curl_setopt($ch, CURLOPT_POST, count($fields));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

	        $result = curl_exec($ch);
	        curl_close($ch);

	        return json_decode($result, true);
    	}
    	$email = htmlspecialchars($_POST["email"]);
	    $username = htmlspecialchars($_POST["username"]);
	    $pass = htmlspecialchars($_POST["pass"]);
	    $conpass = htmlspecialchars($_POST["conpass"]);
	    $password = password_hash($_POST["pass"],PASSWORD_DEFAULT);
	    // Call the function post_captcha
	    $res = post_captcha($_POST['g-recaptcha-response']);

	    if (!$res['success']) {
	        // What happens when the CAPTCHA wasn't checked
	        $captcha= "CAPTCHA requerida";
			header("Location: ../Vistas/InterfazCrearCuenta.php?varC=$captcha");	 
	    }if ($res['success']) {
		    if(validarPass($pass,$conpass)){
		    	// If CAPTCHA is successfully completed...
		        include("../Vistas/header.html");
		        include("../Vistas/mensajeCuenta.html");
		        correoCuenta($email,$username);
		        //echo '<br><p>CAPTCHA was completed successfully!</p><br>';
		        insertarCuenta($username,$password,$email,1);
		        echo "<h3 align=center>Se registró exitosamente tu cuenta</h3>";
			    echo "<h2 align=center>Inicia sesión para registrarte a torneos</h2><br><br>";
			    echo "<a href=../index.php class=btn>Regresar a la pagina principal</a><br><br>" ;   
			    echo "<a href=../Vistas/InterfazLogin.php class=btn>Iniciar Sesión </a></div> </div></div>";
		        include("../Vistas/footer.html");
	    	}else{
	    		$contra= "Contraseña no coincide";
				header("Location: ../Vistas/InterfazCrearCuenta.php?contra=$contra");
	    	}
    	}
	} else { 
		include("../Vistas/header.html");
	    include("../Vistas/mensajeCuenta.html");
		echo "<h3 align=center>No se pudo registrar tu cuenta</h3>";
		echo "<h2 align=center>Intenta de nuevo</h2><br><br>";
		echo "<a href=../index.php class=btn>Regresar a la pagina principal</a><br><br>"   ; 
		echo "<a href=../Vistas/InterfazCrearCuenta.php class=btn>Intentar de nuevo</a></div> </div></div>";
		include("../Vistas/footer.html");
 	} 
    
?>