<?php
    session_start();
    require_once("../Models/modelEditarTorneo.php");
    //$fecha= date('Y-m-d', strtotime($_POST['fecha'])));
	
    $nombreT = htmlspecialchars($_POST["nombreTE"]);
    $preventaT = htmlspecialchars($_POST["preventaTE"]);
    $fechaT = htmlspecialchars($_POST["fechaTE"]);
    $fechaP = htmlspecialchars($_POST["fechaPE"]);
    $fechaC = htmlspecialchars($_POST["fechaCE"]);
    $horaR = htmlspecialchars($_POST["horaRE"]);
    $horaI = htmlspecialchars($_POST["horaIE"]);
    $lugar = htmlspecialchars($_POST["lugarE"]);
    $costo = htmlspecialchars($_POST["costoE"]);
    $cierre = htmlspecialchars($_POST["cierreTE"]);
    $cuentaT = htmlspecialchars($_POST["cuentaTE"]);
    $clabeT = htmlspecialchars($_POST["clabeTE"]);
    $bancoT = htmlspecialchars($_POST["bancoTE"]);
    $beneT = htmlspecialchars($_POST["beneTE"]);
    $torneoActivo = htmlspecialchars($_POST["torneoActivo"]);
    $idTorneo = htmlspecialchars($_POST["idTorneo"]);
    echo editarTorneo($idTorneo,$fechaT,$horaR,$horaI,$lugar,$costo,$cierre,$nombreT,$preventaT,$fechaP,$fechaC,$cuentaT,$clabeT,$bancoT,$beneT,$torneoActivo);
?>