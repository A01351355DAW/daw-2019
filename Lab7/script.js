
function validacion(){
  let str1 = document.getElementById("contraseña").value;
  let str2 = document.getElementById("confirma").value;
  if(str1==str2){
    document.getElementById("demo").innerHTML = "Contraseña validada" ;
  }else{
    document.getElementById("demo").innerHTML = "Lo sentimos, las contraseñas no coinciden";
  }
}

document.getElementById("boton_confirma").onclick = validacion ;


function venta(){
  let precio_raquetas = 1000;
  let precio_tennis = 350;
  let precio_pelotas = 150;
  let num_raquetas = document.getElementById("raquetas").value;
  let num_tennis = document.getElementById("tennis").value;
  let num_pelotas = document.getElementById("pelotas").value;
  let costo = 0;
  costo += num_tennis*precio_tennis;
  costo += num_raquetas*precio_raquetas;
  costo += num_pelotas*precio_pelotas;
  document.getElementById("costo_Total").innerHTML = "Tu compra es de: <br>" + num_tennis + " tennis <br>" + num_pelotas + " pelotas<br>" + num_raquetas + " raquetas<br> El costo total es de: $" + costo;
}

document.getElementById("boton_tienda").onclick = venta;

function quiz(){
  let puntosCap = 0;
  let puntosIronman = 0;
  let puntosHulk = 0;
  let puntosThor = 0;
  if(document.getElementById("color").value == 1){
    puntosCap++;
  }else  if(document.getElementById("color").value == 2){
    puntosIronman++;
  }else if (document.getElementById("color").value == 3){
    puntosHulk++;
  }else{
    puntosThor++;
  }

  if(document.getElementById("loki").value == 1){
    puntosCap++;
  }else  if(document.getElementById("loki").value == 2){
    puntosIronman++;
  }else if (document.getElementById("loki").value == 3){
    puntosHulk++;
  }else{
    puntosThor++;
  }

  if(document.getElementById("palabra").value == 1){
    puntosCap++;
  }else  if(document.getElementById("palabra").value == 2){
    puntosIronman++;
  }else if (document.getElementById("palabra").value == 3){
    puntosHulk++;
  }else{
    puntosThor++;
  }
  
  if(puntosCap>=2){
    document.write("Eres el CAP!");
  }else if(puntosIronman>=2){
    document.write("Eres IRONMAN!");
  }else if(puntosHulk>=2){
    document.write("Eres HULK!");
  }else{
    document.write("Eres THOR");
  }

}

document.getElementById("listo").onclick = quiz;