<?php 
  session_start();
  require_once("model.php");  

  $caso_id = htmlspecialchars($_GET["caso_id"]);

  $titulo = "Editar el caso ".$caso_id;
  include("_header.html");

  $heroe_id = recuperar_heroe($caso_id);
  $editar = 1;
  include("_form_caso.html"); 

  include("_footer.html"); 
?>