-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-06-2020 a las 01:09:45
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lab23`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `REGISTRAR_DEPOSITO_VENTANILLA` (`eNoCuenta` VARCHAR(12), `emonto` NUMERIC(8,2))  BEGIN
	START TRANSACTION;
		INSERT INTO MOVIMIENTOS VALUES(eNoCuenta, 'B', NOW(), emonto);
        UPDATE CLIENTES_BANCA SET Saldo = Saldo + emonto WHERE NoCuenta = eNoCuenta;
        Commit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `REGISTRAR_RETIRO_CAJERO` (`eNoCuenta` VARCHAR(12), `emonto` NUMERIC(8,2))  BEGIN
	START TRANSACTION;
		INSERT INTO MOVIMIENTOS VALUES(eNoCuenta, 'A', NOW(), emonto);
        UPDATE CLIENTES_BANCA SET Saldo = Saldo - emonto WHERE NoCuenta = eNoCuenta;
        Commit;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_banca`
--

CREATE TABLE `clientes_banca` (
  `NoCuenta` varchar(5) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Saldo` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes_banca`
--

INSERT INTO `clientes_banca` (`NoCuenta`, `Nombre`, `Saldo`) VALUES
('001', 'Manuel Rios Maldonado', 8500),
('002', 'Pablo Perez Ortiz', 5000),
('003', 'Luis Flores Alvarado', 8000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `NoCuenta` varchar(5) NOT NULL,
  `ClaveM` varchar(2) NOT NULL,
  `Monto` int(10) NOT NULL,
  `Fecha` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`NoCuenta`, `ClaveM`, `Monto`, `Fecha`) VALUES
('001', 'A', 500, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_movimiento`
--

CREATE TABLE `tipos_movimiento` (
  `ClaveM` varchar(2) NOT NULL,
  `Descripcion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipos_movimiento`
--

INSERT INTO `tipos_movimiento` (`ClaveM`, `Descripcion`) VALUES
('A', 'Retiro Cajero Automatico'),
('B', 'Deposito Ventanilla');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes_banca`
--
ALTER TABLE `clientes_banca`
  ADD PRIMARY KEY (`NoCuenta`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`NoCuenta`,`ClaveM`);

--
-- Indices de la tabla `tipos_movimiento`
--
ALTER TABLE `tipos_movimiento`
  ADD PRIMARY KEY (`ClaveM`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;




/*1.-Revisa el contenido de la tabla clientes_banca desde la ventana que inicializaste como la segunda sesión:

SELECT * FROM CLIENTES_BANCA

¿Que pasa cuando deseas realizar esta consulta?
R= Se muestran los registros creados en el paso previo. 

2.- Revisa el contenido de la tabla clientes_banca desde la ventana que inicializaste como la primera sesión con la siguiente consulta:

SELECT * FROM CLIENTES_BANCA

Revisa el contenido de la tabla clientes_banca desde la ventana que inicializaste como la segunda sesión.

SELECT * FROM CLIENTES_BANCA

¿Qué pasa cuando deseas realizar esta consulta?
R= Solo se muestran los nuevos registros en la ventada donde se realizó la consulta

3.- Intenta con la siguiente consulta desde la segunda sesión:

SELECT * FROM CLIENTES_BANCA where NoCuenta='001'

Explica por qué ocurre dicho evento.
R= Por que cuando se realizó dicha transacción si se realizó el commit

4.-Por último regresa a la ventana que mantiene activa tu primer sesión, agrega el siguiente comando a la pantalla y ejecútalo:

ROLLBACK TRANSACTION PRUEBA2

Revisa nuevamente el contenido de la tabla clientes_banca desde la ventana que inicializaste como la segunda sesión:

SELECT * FROM CLIENTES_BANCA

¿Qué ocurrió y por qué?
R= Se deshizo la transacción, pues eso es lo que hace la sentencia ROLLBACK
5.-¿Para qué sirve el comando @@ERROR revisa la ayuda en línea?
R= Para poder verificar si hubo algun error durente la transacción.
6.-¿Qué hace la transacción?
R=Trata de ingresar datos en la tabla de clientes_banca, en caso de que exista algún error, se hace el ROLLBACK, en caso 
contrario, se ejecuta el commit
7.-¿Hubo alguna modificación en la tabla? Explica qué pasó y por qué.
R=No, se quiso ingresar un dato con llave duplicada, por lo que el la transaccion detectó el error y realizó el ROLLBACK
8.- Una transacción que registre el retiro de una cajero. Nombre del store procedure REGISTRAR_RETIRO_CAJERO que recibe 2 parámetros en NoCuenta y el monto a retirar.

DELIMITER //
CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO( eNoCuenta VARCHAR(12), emonto NUMERIC (8,2))
BEGIN
	START TRANSACTION;
		INSERT INTO MOVIMIENTOS VALUES(eNoCuenta, 'A', NOW(), emonto);
        UPDATE CLIENTES_BANCA SET Saldo = Saldo - emonto WHERE NoCuenta = eNoCuenta;
        Commit;
END;
//
DELIMITER ;
9.-Una transacción que registre el deposito en ventanilla. Nombre del store procedure REGISTRAR_DEPOSITO_VENTANILLA que recibe 2 parámetros en NoCuenta y el monto a depositar.

DELIMITER //
CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA ( eNoCuenta VARCHAR(12), emonto NUMERIC (8,2))
BEGIN
	START TRANSACTION;
		INSERT INTO MOVIMIENTOS VALUES(eNoCuenta, 'B', NOW(), emonto);
        UPDATE CLIENTES_BANCA SET Saldo = Saldo + emonto WHERE NoCuenta = eNoCuenta;
        Commit;
END; */