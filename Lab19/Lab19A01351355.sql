--1
SELECT DATE_FORMAT("01/01/00", "%d %M %Y");
SELECT SUM(cantidad) as CantidadTotal, SUM(costo*cantidad) as ImporteTotal
FROM Entregan as E, Materiales as M
WHERE E.clave = M.clave
AND Fecha between '01/01/97' and '31/12/97';

--2
SELECT RazonSocial, COUNT(*) as NumEntregas, SUM(cantidad*costo) as ImporteTotal
FROM Entregan E, Proveedores P, Materiales M
WHERE E.rfc = P.rfc
AND E.clave = M.clave
GROUP BY RazonSocial;

--3
select M.clave, descripcion, SUM(cantidad) as TotalEntregado, min(cantidad) as MinimoEntregado, MAX(cantidad) as MaximoEntregado, SUM(cantidad*costo) as ImporteTotal
FROM Materiales M, Entregan E
WHERE M.clave = E.clave
GROUP BY Clave
HAVING TotalEntregado > 400;

--4
SELECT RazonSocial, AVG(cantidad) as CantidadPromedio, Descripcion, M.Clave
FROM Entregan E, Proveedores P, Materiales M
WHERE E.rfc = P.rfc
AND E.Clave = M.Clave
HAVING CantidadPromedio > 500;

--5
SELECT RazonSocial, AVG(cantidad) as CantidadPromedio, Descripcion, M.Clave
FROM Entregan E, Proveedores P, Materiales M
WHERE E.rfc = P.rfc
AND E.Clave = M.Clave
HAVING CantidadPromedio < 370 OR CantidadPromedio > 450;

--inserta cinco nuevos materiales.
INSERT INTO Materiales 
VALUES(1440, "Varilla 4", 205, 2*clave/1000);
INSERT INTO Materiales 
VALUES(1450, "PintuSayer", 600, 2*clave/1000);
INSERT INTO Materiales 
VALUES(1460, "PVC 3", 88, 2*clave/1000);
INSERT INTO Materiales 
VALUES(1470, "Adobe 13", 70, 2*clave/1000);
INSERT INTO Materiales 
VALUES(1480, "Rejilla", 75, 2*clave/1000);

SELECT * FROM materiales;

--6
SELECT clave, descripcion
FROM materiales
WHERE clave NOT IN (SELECT clave FROM entregan);

--7
SELECT razonsocial 
FROM proveedores as p, entregan as e, proyectos as pr
WHERE p.RFC = e.RFC AND pr.Numero = e.Numero
AND denominacion = 'Queretaro Limpio'
AND razonsocial IN (   SELECT razonsocial 
			FROM proveedores as p, entregan as e, proyectos as pr
			WHERE p.RFC = e.RFC AND pr.Numero = e.Numero
			AND pr.Denominacion = 'Vamos Mexico')

--8
SELECT descripcion 
from materiales, entregan, proyectos p
WHERE entregan.Clave = materiales.Clave AND p.Numero = entregan.Numero
AND descripcion NOT IN (SELECT descripcion 
			from materiales, entregan, proyectos p
			WHERE entregan.Clave = materiales.Clave AND p.Numero = entregan.Numero
			AND p.Denominacion = 'CIT Yucatan')


--9
SELECT razonsocial, AVG (Cantidad) as 'Promedio'
FROM proveedores as p, entregan as e 
WHERE p.RFC = e.RFC
GROUP BY razonsocial
HAVING AVG(e.Cantidad) >(SELECT AVG (Cantidad)
					 FROM entregan as e 
					 WHERE  RFC = 'VAGO780901')

--10
SELECT proveedores.rfc, razonsocial
FROM proveedores, proyectos,entregan
WHERE proyectos.Numero = entregan.Numero AND proveedores.RFC = entregan.RFC
AND proyectos.Denominacion = 'Infonavit Durango'
AND entregan.Fecha BETWEEN '2000-01-01' AND '2000-12-31'
GROUP BY proveedores.rfc
HAVING SUM(cantidad) > ( SELECT SUM(cantidad) 
						FROM proveedores, proyectos,entregan
						WHERE proyectos.Numero = entregan.Numero AND proveedores.RFC = entregan.RFC
						AND proyectos.Denominacion = 'Infonavit Durango'
						AND entregan.Fecha BETWEEN '2001-01-01' AND '2001-12-31')

